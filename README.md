# Laravel JsonApi
This package is built for the Laravel framework and provides basic classes
to help you building a REST API following the json:api standard.

## Laravel compatibility
 * Laravel 5.3

## Features
 * Base controllers
  * **DocumentResourceController**:
  * **CollectionResourceController**:
  * **StoreResourceController**:
  * *ControllerResourceController*: TODO
  * **DefaultResourceController**:
  * **OneToOneResourceController**:
 * Specification features:
  * **Pagination**
  * **Filtering**
  * **Includes**
  * *Sorting: TODO*
 * Web Service Maturity
  * **HATEOAS**:
 * Validation:
  * **Request body validation**

## Manual
Please follow the [manual](doc/index.md).

## Resources
 * REST API Design Rulebook (Mark Masse)

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

## Mailinglist
https://lists.ffnw.de/mailman/listinfo/netmon-dev

# License
See [License](LICENSE.txt)
