<?php

namespace ApiServer\JsonApi\Http\Controllers;

use ApiServer\JsonApi\Traits\DocumentResourceTrait;

abstract class DocumentResourceController extends JsonApiController
{
    use DocumentResourceTrait;

    public function __construct() {
        $this->initDocumentResourceTrait();

        parent::__construct();
    }
}

?>
