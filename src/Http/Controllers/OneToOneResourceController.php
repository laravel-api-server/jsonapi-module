<?php

namespace ApiServer\JsonApi\Http\Controllers;

use ApiServer\JsonApi\Traits\StoreResourceTrait;

abstract class OneToOneResourceController extends JsonApiController
{
    use StoreResourceTrait;

    protected $siblingModel;

    public function __construct() {
        $this->siblingModel = $this->siblingModel();

        $this->initStoreResorceTrait();

        parent::__construct();
    }

    public abstract function siblingModel();

    /**
     * Returns a new instance of the model associated to this controller.
     */
    public function modelInstance($siblingId = null) {
        $model = $this->model;

        if(!is_null($siblingId)) {
            $siblingModel = $this->siblingModel;
            $siblingModel = $siblingModel::findOrFail($siblingId);
            $relationName = (new \ReflectionClass($model))->getShortName();
            return $siblingModel->{$relationName}();
        }
        return new $model;
    }
}
