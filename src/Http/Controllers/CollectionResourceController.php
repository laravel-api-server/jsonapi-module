<?php

namespace ApiServer\JsonApi\Http\Controllers;

use ApiServer\JsonApi\Traits\CollectionResourceTrait;

abstract class CollectionResourceController extends JsonApiController
{
    use CollectionResourceTrait;

    public function __construct() {
        $this->initCollectionResourceTrait();

        parent::__construct();
    }
}

?>
