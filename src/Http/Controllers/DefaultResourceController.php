<?php

namespace ApiServer\JsonApi\Http\Controllers;

use ApiServer\JsonApi\Traits\StoreResourceTrait;
use ApiServer\JsonApi\Traits\CollectionResourceTrait;

abstract class DefaultResourceController extends JsonApiController
{
    use StoreResourceTrait;
    use CollectionResourceTrait;

    public function __construct() {
        $this->initStoreResorceTrait();
        $this->initCollectionResorceTrait();

        parent::__construct();
    }
}

?>
