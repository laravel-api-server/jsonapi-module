<?php

Route::group([
    'namespace' => '\ApiServer\JsonApi\Http\Controllers',
    'middleware' => 'cors'], function()
{
	/**
	 * Routes using JWT auth
	 */
	Route::group(['middleware' => ['auth.jwt']], function () {
		Route::get('/', 'RootController@index');
	});
});

?>
