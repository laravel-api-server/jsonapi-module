<?php namespace ApiServer\JsonApi;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

use ApiServer\JsonApi\Serializers\BasicSerializer;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router) {
        BasicSerializer::setContainer($this->app);

        // boot cors middleware
        $router->aliasMiddleware(
            'cors',
            'Barryvdh\Cors\HandleCors'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require __DIR__ . '/Http/routes.php';

        $this->app->register('Barryvdh\Cors\ServiceProvider');

        //register all the service provider this user needs
        $this->app->register('ApiServer\JsonApi\Providers\ResponseMacroServiceProvider');
        $this->app->register('ApiServer\JsonApi\Providers\ErrorHandlerServiceProvider');
    }
}
