<?php

function add_serializer_relation($serializerClass, $relationName, $relationClosure) {
    if(!class_exists($serializerClass)) {
        throw new \Exception("{$serializerClass} does not exist.");
    } elseif( !is_object($relationClosure)
        || !($relationClosure instanceof \Closure)
    ) {
        throw new \Exception("Not a closure");
    }

    config([
        "relations.{$serializerClass}.{$relationName}" => $relationClosure
    ]);
}

function get_serializer_relation($serializerClass, $relationName) {
    $relationClosure = config("relations.{$serializerClass}.{$relationName}");

    if( !is_object($relationClosure)
        || !($relationClosure instanceof \Closure)
    ) {
        throw new \BadMethodCallException(
            "Runtime method {$relationName} on class "
            ."{$serializerClass} not found."
        );
    }

    return $relationClosure;
}
