<?php

namespace ApiServer\JsonApi\Exceptions\Handler;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use Tobscure\JsonApi\Exception\Handler\ResponseBag;
use ApiServer\ErrorHandler\Contracts\ExceptionHandler;

use Illuminate\Database\Eloquent\RelationNotFoundException;
use ApiServer\JsonApi\Exceptions\Exceptions\BodyValidationException;
use ApiServer\JsonApi\Exceptions\Exceptions\InvalidFilterException;

class BadRequestExceptionHandler extends ExceptionHandler {
    /**
     * {@inheritdoc}
     */
    protected function managesRoute(Route $route): bool {
        return(strpos($route->getName(), 'jsonapi.') === 0);
    }

    /**
     * {@inheritdoc}
     */
    protected function managesException(Exception $e): bool {
        return (    $e instanceof RelationNotFoundException
                    || $e instanceof BodyValidationException
                    || $e instanceof InvalidFilterException
        );
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Exception $e, Request $request): Response {
        $errors = [];

        if($e instanceof RelationNotFoundException) {
            $errors[] = [
                    'detail' => $e->getMessage()
            ];
        }

        if( $e instanceof BodyValidationException
            || $e instanceof InvalidFilterException) {
            $messageBag = $e->getErrors();
            foreach($messageBag->getMessages() as $key=>$errorMessage) {
                $errors[] = [
                    'code' => "$key",
                    'detail' => $errorMessage
                ];
            }
        }

        return response()->jsonapiErrorResponse(
            $status,
            new ResponseBag(400, $errors)
        );
    }
}
