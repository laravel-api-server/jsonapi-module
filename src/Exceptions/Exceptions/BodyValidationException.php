<?php

namespace ApiServer\JsonApi\Exceptions\Exceptions;

use Exception;
use Illuminate\Support\MessageBag;

class BodyValidationException extends Exception
{
    protected $code = 400;
    protected $validationErrors;

    public function __construct (
        $message = "Request body validation failed.",
        MessageBag $validationErrors,
        Exception $previous = NULL
    ) {
        parent::__construct($message, $this->code, $previous);
        $this->validationErrors = $validationErrors;
    }

    public function getValidationErrors() {
        return $this->validationErrors;
    }
}
