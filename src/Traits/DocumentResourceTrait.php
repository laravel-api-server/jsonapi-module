<?php

namespace ApiServer\JsonApi\Traits;

use Gate;

use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Resource;

trait DocumentResourceTrait {
    protected function initDocumentResourceTrait() {

    }

    protected function documentResponse($model, $statuscode = 200) {
        // Create a new resource
        $resource = new Resource($model, $this->serializerInstance());
        // Add relations that have been requested to be included
        $resource->with($this->includes);

        // Create a new json:api document with the resource as data.
        $document = new Document($resource);

        //add top level links
        if(Gate::allows('index', $model))
            $document->addLink('index', $this->baseURI);
        if(Gate::allows('store', $model))
            $document->addLink('create', $this->baseURI);

        //on success: return the newly created resource with statuscode 201
        return response()->json($document)->setStatusCode($statuscode);
    }
}

?>
