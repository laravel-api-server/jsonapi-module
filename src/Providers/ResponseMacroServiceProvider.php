<?php

namespace ApiServer\JsonApi\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;
use Tobscure\JsonApi\Exception\Handler\ResponseBag;
class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param  ResponseFactory  $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('noContent', function () use ($factory) {
            return $factory->make("", 204);
        });

        $factory->macro('created', function () use ($factory) {
            return $factory->make("", 201);
        });

        $factory->macro('item', function ($item) use ($factory) {
            return $factory->json($item->toArray());
        });

        $factory->macro('collection', function ($collection) use ($factory) {
            return $factory->json($collection->toArray());
        });

        $factory->macro('jsonapiErrorResponse', function (ResponseBag $responseBag) use ($factory) {
            $document = new \Tobscure\JsonApi\Document();
            $document->setErrors($responseBag->getErrors());
            return $factory->make($document, $responseBag->getStatus());
        });
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
