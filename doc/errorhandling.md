We use the [tobscure/jsonapi](https://github.com/tobscure/json-api)-package for
errorhandling. Just overwrite the `render($request, Exception $exception)`-method in your
`app/Exceptions/Handler.php` with the following to enable error responses in
json:api format:

```
public function render($request, Exception $exception)
{
    // if the clients wants json, then use JsonApi ErrorHandler
    if($request->wantsJson() || $request->ajax()) {
        $errorHandler = new \Tobscure\JsonApi\ErrorHandler\ErrorHandler();

        $response = $errorHandler->handle($e);

        $document = new \Tobscure\JsonApi\Document;
        $document->setErrors($response->getErrors());
        return response($document, $response->getStatus());
    }

    // use default error handling otherwise
    return parent::render($request, $exception);
}
```
